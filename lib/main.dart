import 'package:flutter/material.dart';
import 'package:flutter_app_world_clock/pages/choose_location.dart';
import 'package:flutter_app_world_clock/pages/home.dart';
import 'package:flutter_app_world_clock/pages/loading.dart';
void main() =>
  runApp(MaterialApp(
      initialRoute: 'homescreen',
      routes: {
        'homescreen':(context)=> Home(),
        'lodingscreen':(context)=> Loading(),
        'locationscreen':(context)=>ChooseLocation(),
  }
));
