import 'package:flutter/material.dart';
class ChooseLocation extends StatefulWidget {
  @override
  _ChooseLocationState createState() => _ChooseLocationState();
}

class _ChooseLocationState extends State<ChooseLocation> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      appBar: AppBar(
        backgroundColor: Colors.grey[800],
        centerTitle:true,
        title: Text('Choose Location',
        style: TextStyle(
          fontSize: 20,
          color: Colors.white
        ),),
      ),
      body:Text('choose location screen')
    );
  }
}

